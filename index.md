---
layout: default
title:  "Pratik Agrawal"
---


Hi there, I'm Pratik.
  
I'm software Engineer.   

I occasionally read (mostly non-fiction) and write(blog) too. 

I studied [Computer Science](http://www.msubaroda.ac.in) at Vadodara, India. I have a [Diploma](http://www.nirmauni.ac.in) in computer engineering.

![me](images/web.jpg)



The best way to contact me(is via email (pratik4fp [at] gmail [dot] com) or
Twitter [@pratik_jack](https://twitter.com/pratik_jack). 




